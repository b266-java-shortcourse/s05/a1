package com.zuitt.example;

public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact();
        contact1.setName("Iris");
        contact1.setContactNumber("09553194178");
        contact1.setAddress("Quezon City");

        Contact contact2 = new Contact("Jerisha", "09104731109", "QC, Manila, Philippines");

        phonebook.setContacts(contact1);
        phonebook.setContacts(contact2);

        if (phonebook.getContacts().isEmpty()) {
            System.out.println("The phonebook is currently empty.");
        } else {
            phonebook.getContacts().forEach((contact) -> {
                System.out.println("Name: " + contact.getName());
                System.out.println("Contact number: " + contact.getContactNumber());
                System.out.println("Address: " + contact.getAddress());
            });
        }
    }
}