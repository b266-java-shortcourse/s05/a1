package com.zuitt.example;

public class Contact {

//   Step 3 properties
    private String name;
    private String contactNumber;
    private String address;

//   Step 4 constructor
    public Contact(){};

    Contact(String name, String contactNumber, String address){
        this.name = name;
        this.contactNumber = contactNumber;
        this.address = address;
    }

//  Step 5 getter

    public String getName(){
        return this.name;
    }
    public String getContactNumber(){
        return this.contactNumber;
    }
    public String getAddress(){
        return this.address;
    }

//  Step 5 setter
    public void setName(String name){
        this.name = name;
    }
    public void setContactNumber(String contactNumber){
        this.contactNumber = contactNumber;
    }
    public void setAddress(String address) {
        this.address = address;
    }


}
