package com.zuitt.example;

import java.util.ArrayList;

public class Phonebook {
// Step 6
    public ArrayList<Contact> contacts = new ArrayList<Contact>();

    public Phonebook() {};

    public Phonebook(Contact contact){
        this.contacts.add(contact);
   }

//    setter
    public void setContacts(Contact contact){
       this.contacts.add(contact);
    }

//    getter
    public ArrayList<Contact> getContacts(){
       return contacts;
    }


}
